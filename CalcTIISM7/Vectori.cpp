#include "Vectori.h"
#include <iostream>
using namespace std;
#include "FunctiiSimple.h"

void PrimeleNumerePrime(int prime[], int &lPrime)
{
	prime[0] = 1;
	for (int i = 1; i < lPrime; i++)
	{
		prime[i] = UrmatorulNumarPrim(prime[i-1]);
	}
}

void Afisare(int a[], int n)
{
	for (int i = 0; i < n; i++)
	{
		cout << a[i] << " ";
	}
	cout << endl;
}



int SumaValorilorCuIndicePrim(int a[], int n)
{
	int S = 0;
	for (int i = 1; i < n; i++) {
		S += a[i];
	}
	return S;
}

void MutaValorileNegativeLaInceput(int a[], int n)
{
	int st = 0, dr = n - 1;
	while (st <= dr) {
		while (st < n && a[st] < 0) {
			st++;
		}
		while (dr >= 0 && a[dr] > 0) {
			dr--;
		}
		if (st < dr) {
			swap(a[st], a[dr]);
		}
		st++;
		dr--;
	}
}

void GenerareRandom(int a[], int & n, int A, int B)
{
	for (int i = 0; i < n; i++)
	{
		a[i] = rand()%(B-A+1) + A;
	}
}

