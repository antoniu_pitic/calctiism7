#include "FunctiiSimple.h"


int UrmatorulNumarPrim(int n)
{
	do {
		n++;
	} while (!Prim(n));

	return n;
}

bool Prim(int n)
{
	for (int d = 2; d <= n / 2; d++) {
		if (n%d == 0) {
			return false;
		}
	}
	return true;
}
